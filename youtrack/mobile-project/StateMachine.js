/*jshint esversion: 6 */

const entities = require('@jetbrains/youtrack-scripting-api/entities');
const helper = require('./helper');

// ctx.issue.fields.Assignee = entities.User.findByLogin("d.nosarev");
// ctx.issue.fields.Assignee = ctx.issue.reporter;

exports.rule = entities.Issue.stateMachine({
    title: 'Status state-machine',
    fieldName: 'State',
    states: {
        'Submitted': {
            initial: true,
            transitions: {
                Open: {
                    targetState: 'Open',
                    guard: function (ctx) {
                        helper.checkIssueIsReported(ctx);
                        helper.checkIssueHasAssignee(ctx);
                        return true;
                    }
                },
                'Wont Fix': {
                    targetState: 'Wont Fix',
                    guard: function (ctx) {
                        helper.checkIssueIsReported(ctx);
                        return true;
                    }
                },
                'Duplicate': {
                    targetState: 'Duplicate',
                    guard: function (ctx) {
                        helper.checkIssueIsReported(ctx);
                        return true;
                    }
                },
                'Already Done': {
                    targetState: 'Already Done',
                    guard: function (ctx) {
                        helper.checkIssueIsReported(ctx);
                        return true;
                    }
                }
            }
        },

        'Open': {
            transitions: {
                'In Progress': {
                    guard: function (ctx) {
                        // FIXME: Add logic about release
                        //workflow.check(helper.    findReleaseTask(ctx.issue).get(0), "You must add release to task!");
                        return true;
                    },
                    targetState: 'In Progress'
                },
                'Hold': {
                    targetState: 'Hold'
                }
            }
        },

        'Already Done': {
            onExit: function (ctx) {
                ctx.issue.fields.Assignee = ctx.issue.reporter;
            },
            transitions: {
                'Submitted': {
                    targetState: 'Submitted'
                }
            }
        },

        'Wont Fix': {
            transitions: {
                'Submitted': {
                    targetState: 'Submitted'
                }
            }
        },

        'Duplicate': {
            transitions: {
                'Submitted': {
                    targetState: 'Submitted'
                }
            }
        },

        'Hold': {
            transitions: {
                'Open': {
                    targetState: 'Open'
                },
                'In Progress': {
                    targetState: 'In Progress'
                },
                'Ready to test': {
                    targetState: 'Ready to test'
                },
                'Testing': {
                    targetState: 'Testing'
                }
            }
        },

        'In Progress': {
            transitions: {
                'Ready to test': {
                    targetState: 'Ready to test'
                },
                'Hold': {
                    targetState: 'Hold'
                },
                'Already Done': {
                    targetState: 'Already Done'
                }
            }
        },

        'Ready to test': {
            transitions: {
                'Testing': {
                    targetState: 'Testing'
                },
                'In Progress': {
                    targetState: 'In Progress'
                },
                'Hold': {
                    targetState: 'Hold'
                },
                'Already Done': {
                    targetState: 'Already Done'
                }
            }
        },

        'Testing': {
            transitions: {
                'Tested': {
                    targetState: 'Testing'
                },
                'In Progress': {
                    targetState: 'In Progress'
                },
                'Hold': {
                    targetState: 'Hold'
                },
                'Already Done': {
                    targetState: 'Already Done'
                }
            }
        },

        'Tested': {
            transitions: {
                'Waiting for release': {
                    targetState: 'Waiting for release'
                },
                'Already Done': {
                    targetState: 'Already Done'
                }
            }
        },

        'Waiting for release': {
            transitions: {
                'Send to market': {
                    targetState: 'Send to market'
                },
                'Already Done': {
                    targetState: 'Already Done'
                }
            }
        },

        'Send to market': {
            transitions: {
                'Released': {
                    targetState: 'Released'
                }
            }
        },

        'Released': {
            transitions: {
                'Verified': {
                    targetState: 'Verified'
                }
            }
        },

        'Verified': {transitions: {}}
    },
    requirements: {
        Priority: {
            type: entities.EnumField.fieldType,
            'Show-Stopper': {},
            Critical: {},
            Major: {},
            Normal: {},
            Minor: {}
        },
        Type: {
            type: entities.EnumField.fieldType,
            Epic: {},
            UserStory: {},
            Feature: {},
            Bug: {},
            Task: {},
        },
        State: {
            type: entities.State.fieldType,
            Submitted: {},
            Open: {},
            'Already Done': {},
            'Wont Fix': {},
            Duplicate: {},
            Hold: {},
            'In Progress': {},
            'Ready to test': {},
            Testing: {},
            Tested: {},
            'Waiting for release': {},
            'Send to market': {},
            Released: {},
            Verified: {}
        },
        Assignee: {
            type: entities.User.fieldType
        },
        'Bug Type': {
            type: entities.EnumField.fieldType,
            multi : true
        }
    }
});

