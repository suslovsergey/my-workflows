/*jshint esversion: 6 */

const workflow = require('@jetbrains/youtrack-scripting-api/workflow');
const entities = require('@jetbrains/youtrack-scripting-api/entities');

exports.checkIssueIsReported = function (ctx) {
    workflow.check(ctx.issue.isReported, "You can't change state before task is reported");
};

exports.checkIssueHasAssignee = function (ctx) {
    workflow.check(ctx.issue.fields.Assignee, "You Must set Assignee");
};

exports.types = {
    type: entities.EnumField.fieldType,
    Epic: {},
    UserStory: {},
    Feature: {},
    Bug: {},
    Task: {}
};

exports.checkLinks = function (ctx, subTask, isSubtask, allowed) {
    if (!subTask.isReported) return;

    workflow.check(
        allowed.indexOf(subTask.fields.Type.name) !== -1,
        "Issue - " + ctx.issue.id + ", can't  be linked as " + (isSubtask ? "subtask of" : "parent for") + " " + subTask.id + ". You cant link with  only: [" + allowed.join(", ") + "]"
    );
};

exports.checkLinksGuard = function (ctx, typename) {
    return ctx.issue.fields.Type.name === typename &&
        (ctx.issue.links['subtask of'].size > 0 || ctx.issue.links['parent for'].size > 0);

};

exports.findReleaseTask = function (issue) {
    const subTasks = issue.links['subtask of'];
    var releaseTaskArray = issue.links['of release'];

    if (subTasks) {
        subTasks.forEach(function (_issue) {
            const _releaseTaskArray = findReleaseTask(_issue);
            if (_releaseTaskArray) {
                _releaseTaskArray.forEach(function (_releaseTask) {
                    releaseTaskArray.add(_releaseTask);
                })
            }
        });
    }

    if (releaseTaskArray) return releaseTaskArray;
    return [];
};

