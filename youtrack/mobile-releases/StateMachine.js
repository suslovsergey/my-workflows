/*jshint esversion: 6 */


const entities = require('@jetbrains/youtrack-scripting-api/entities');
const helper = require('./helper');

// ctx.issue.fields.Assignee = entities.User.findByLogin("d.nosarev");
// ctx.issue.fields.Assignee = ctx.issue.reporter;

exports.rule = entities.Issue.stateMachine({
    title: 'Status state-machine V2',
    fieldName: 'State',
    states: {
        'Submitted': {
            initial: true,
            transitions: {
                Open: {
                    targetState: 'Open',
                    guard: function (ctx) {
                        helper.checkIssueIsReported(ctx);
                        return true;
                    }
                }
            }
        },

        'Open': {
            transitions: {
                'In Progress': {
                    targetState: 'In Progress'
                }
            }
        },

        'In Progress': {
            transitions: {
                'Testing': {
                    targetState: 'Testing'
                }
            }
        },

        'Testing': {
            transitions: {
                'Tested': {
                    targetState: 'Tested'
                }
            }
        },


        'Tested': {
            transitions: {
                'Send to Market': {
                    targetState: 'Send to Market'
                }
            }
        },

        'Send to Market': {
            transitions: {
                'In Production': {
                    targetState: 'In Production'
                }
            }
        },

        'In Production': {
            transitions: {
                'Verified': {
                    targetState: 'Verified'
                }
            }
        },

        'Verified': {
            transitions: {}
        }
    }


});

